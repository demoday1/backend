const { writeFileSync } = require('fs');

const createBaseFile = () => `
stages:
  - publish
`;

const createBaseFileWithDeployment = () => `
image: ubuntu:18.04
stages:
  - publish
`;

const createEmptyJob = () => `
publish:empty:
  stage: publish
  script:
    - echo 'no apps affected!'
`;

const createJob = (service) => `
publish:${service}:
  stage: publish
  image: docker
  variables:
    SERVICE: ${service}
  services:
    - name: docker:dind
      alias: docker
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build --pull --cache-from $CI_REGISTRY_IMAGE --build-arg SERVICE=${service} --tag $CI_REGISTRY_IMAGE/${service}:$CI_COMMIT_REF_SLUG .
    - docker push $CI_REGISTRY_IMAGE/${service}:$CI_COMMIT_REF_SLUG
`;

const createCIFile = (projects) => {
  if (!projects.length) {
    return createBaseFile().concat(createEmptyJob());
  }

  const publish = createBaseFileWithDeployment().concat(
    projects.map(createJob).join('\n')
  );

  return publish;
};

const createDynamicGitLabFile = () => {
  const [stringifiedAffected] = process.argv.slice(2);
  const { projects } = JSON.parse(stringifiedAffected);

  console.log('projects', projects);
  /*
  {
    projects: ['app1', 'app2'],
  };

  */
  const content = createCIFile(projects);

  writeFileSync('dynamic-gitlab-ci.yml', content);
};

createDynamicGitLabFile();
