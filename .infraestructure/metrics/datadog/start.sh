# helm install datadog -f ./datadog-values.yaml --set datadog.site='us5.datadoghq.com' --set datadog.apiKey='1aa84189803d2ff063aea45a749f9c96' datadog/datadog 



helm install datadog -n datadog \
--set datadog.site='us5.datadoghq.com' \
--set datadog.clusterName='withbeer-k8s' \
--set datadog.clusterAgent.replicas='2' \
--set datadog.clusterAgent.createPodDisruptionBudget='true' \
--set datadog.kubeStateMetricsEnabled=true \
--set datadog.kubeStateMetricsCore.enabled=true \
--set datadog.logs.enabled=true \
--set datadog.logs.containerCollectAll=true \
--set datadog.apiKey='xxxx' \
--set datadog.processAgent.enabled=true \
 datadog/datadog --create-namespace


