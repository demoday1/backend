FROM node:18 as build
ARG SERVICE
ENV SERVICE ${SERVICE}
WORKDIR /home/usr/app
COPY . .
RUN npm install
RUN npx nx build ${SERVICE}


FROM node:18
ARG SERVICE
ENV SERVICE ${SERVICE}
WORKDIR /home/usr/app
COPY --from=build /home/usr/app .
CMD node /home/usr/app/dist/apps/$SERVICE/main.js
