/* eslint-disable @typescript-eslint/no-explicit-any */
import { HttpService } from '@nestjs/axios';
import {
  Injectable,
  Logger,
  ServiceUnavailableException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IResponse } from './interfaces/response.interface';
import { AxiosError } from 'axios';

export enum CoreMSEnum {
  TRANSFER = 'transfer',
}

export interface MSApiManagerInterface {
  get<T>(ms: CoreMSEnum, url: string): Promise<T>;
  post<T>(ms: CoreMSEnum, url: string, body: any): Promise<T>;
  put<T>(ms: CoreMSEnum, url: string, body: any): Promise<T>;
  delete<T>(ms: CoreMSEnum, url: string): Promise<T>;
}

@Injectable()
export class MSApiManager implements MSApiManagerInterface {
  private readonly logger = new Logger(MSApiManager.name);

  constructor(
    private readonly config: ConfigService,
    private readonly http: HttpService
  ) {
    this.http.axiosRef.defaults.timeout = 15000;
  }

  async get<T>(ms: CoreMSEnum, url: string): Promise<T> {
    try {
      const urlComplete = this.getURL(ms, url);
      const res = await this.http.get<T>(urlComplete).toPromise();
      return res.data;
    } catch (error) {
      this.handleError(ms, error);
    }
  }

  async post<T>(ms: CoreMSEnum, url: string, body: any): Promise<T> {
    try {
      const urlComplete = this.getURL(ms, url);
      const res = await this.http.post<T>(urlComplete, body).toPromise();
      return res.data;
    } catch (error) {
      this.handleError(ms, error);
    }
  }

  async put<T>(ms: CoreMSEnum, url: string, body: any): Promise<T> {
    try {
      const urlComplete = this.getURL(ms, url);
      const res = await this.http.put<T>(urlComplete, body).toPromise();
      return res.data;
    } catch (error) {
      this.handleError(ms, error);
    }
  }

  async delete<T>(ms: CoreMSEnum, url: string): Promise<T> {
    try {
      const urlComplete = this.getURL(ms, url);
      const res = await this.http.delete<T>(urlComplete).toPromise();
      return res.data;
    } catch (error) {
      this.handleError(ms, error);
    }
  }

  private getURL(ms: CoreMSEnum, url: string): string {
    const str = `microservices.${ms}`;
    const baseURL = this.config.get<string>(str);
    if (!baseURL) {
      this.logger.error(`ENV: Microservice ${ms} not found`);
      throw new Error(`Microservice ${ms} not found`);
    }
    return `${baseURL}${url}`;
  }

  private handleError(ms: CoreMSEnum, error: any): never {
    this.logger.error(error);

    if (error.isAxiosError) {
      const axiosError = error as AxiosError;
      const { status, data } = axiosError.response;
      const { statusCode, message }: IResponse<any> = data as IResponse<any>;

      this.logger.error(`Request failed with status: ${status} - ${message}`);
      throw {
        statusCode,
        message,
        httpStatus: status,
      };
    } else if (
      error.code === 'ECONNREFUSED' ||
      error.code === 'ECONNABORTED' ||
      error.code === 'ETIMEDOUT' ||
      error.code === 'ENOTFOUND' ||
      error.code === 'ECONNRESET'
    ) {
      throw new ServiceUnavailableException(
        `Microservice ${ms} is not available`
      );
    } else {
      this.logger.error('Request failed. No response received.');
      throw new Error('Request failed. No response received.');
    }
  }
}
