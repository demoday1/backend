export default () => ({
  microservices: {
    transfers: process.env.TRANSFER_MS ?? 'http://localhost:9001/api',
  },
});
