import { Module } from '@nestjs/common';
import { MSApiManager } from './ms-api.service';
import { ConfigModule } from '@nestjs/config';
import { HttpModule } from '@nestjs/axios';
import config from './config/config';
@Module({
  imports: [ConfigModule.forRoot({ load: [config] }), HttpModule],
  providers: [MSApiManager],
  exports: [MSApiManager],
})
export class CoreModule {}
