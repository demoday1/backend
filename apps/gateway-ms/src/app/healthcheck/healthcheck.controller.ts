import { Controller, Get } from '@nestjs/common';
import { HealthCheck, HealthCheckService } from '@nestjs/terminus';

@Controller('health')
export class HealthcheckController {
  constructor(private health: HealthCheckService) {}

  @Get('')
  @HealthCheck()
  getMS() {
    return this.health.check([]);
  }
}
