import { Controller, Get } from '@nestjs/common';
import { HealthCheck, HealthCheckService } from '@nestjs/terminus';
import { Span } from 'nestjs-ddtrace';

@Controller('health')
@Span()
export class HealthcheckController {
  constructor(private health: HealthCheckService) {}

  @Get('')
  @HealthCheck()
  getMS() {
    return this.health.check([]);
  }
}
