import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TerminusModule } from '@nestjs/terminus';

import { LoggerModule } from 'nestjs-pino';
import { DatadogTraceModule } from 'nestjs-ddtrace';
import { HealthcheckController } from './healthcheck/healthcheck.controller';
import config from '../environments/config';
import { TransferModule } from './transfer/transfer.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true, load: [config] }),
    DatadogTraceModule.forRoot(),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => {
        const env = config.get<string>('NODE_ENV');
        const level = env !== 'production' ? 'debug' : 'trace';
        const transport =
          env !== 'production' ? { target: 'pino-pretty' } : null;
        return {
          pinoHttp: {
            level: level,
            transport: transport,
          },
        };
      },
    }),
    TerminusModule,
    TransferModule,
  ],
  controllers: [HealthcheckController],
  providers: [],
})
export class AppModule {}
