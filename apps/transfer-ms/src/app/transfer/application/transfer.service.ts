import { Inject, Injectable, Logger } from '@nestjs/common';

import { TRANSFER_TOKEN, TransferPort } from '../domain/transfer.port';
import { TransferCommand } from '../domain/transfer.command';
import { Span } from 'nestjs-ddtrace';

@Span()
@Injectable()
export class TransferService implements TransferPort {
  private readonly logger = new Logger(TransferService.name);
  constructor(@Inject(TRANSFER_TOKEN) private readonly port: TransferPort) {}

  sendMoney(command: TransferCommand) {
    this.logger.log(`Send command ${JSON.stringify(command)}`);
    return this.port.sendMoney(command);
  }
}
