import { TransferCommand } from './transfer.command';

export interface TransferPort {
  sendMoney(command: TransferCommand);
}

export const TRANSFER_TOKEN = ' TRANSFER_TOKEN';
