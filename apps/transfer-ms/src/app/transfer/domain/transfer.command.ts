export interface TransferProps {
  from: string;
  to: string;
  amount: number;
  description: string;
}

export class TransferCommand {
  readonly from: string;
  readonly to: string;
  readonly amount: number;
  readonly description: string;

  constructor(props: TransferProps) {
    this.from = props.from;
    this.to = props.to;
    this.amount = props.amount;
    this.description = props.description;
  }
}
