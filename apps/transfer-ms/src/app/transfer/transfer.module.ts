import { Module } from '@nestjs/common';

import { TransferAdapter } from './infraestructure/transfer.adapter';
import { TransferService } from './application/transfer.service';
import { TransferController } from './infraestructure/transfer.controller';
import { TRANSFER_TOKEN } from './domain/transfer.port';

@Module({
  imports: [],
  controllers: [TransferController],
  providers: [
    TransferService,
    {
      provide: TRANSFER_TOKEN,
      useClass: TransferAdapter,
    },
  ],
  exports: [],
})
export class TransferModule {}
