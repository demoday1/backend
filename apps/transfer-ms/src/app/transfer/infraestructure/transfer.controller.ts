import { Body, Controller, Post, Headers, Logger, Get } from '@nestjs/common';

import { TransferService } from '../application/transfer.service';
import { TransferCommand } from '../domain/transfer.command';
import { TransferDto } from './dto/transfer.dto';
import { Span, TraceService } from 'nestjs-ddtrace';

@Span()
@Controller('transfer')
export class TransferController {
  private readonly logger = new Logger(TransferController.name);
  constructor(
    private readonly service: TransferService,
    private readonly traceService: TraceService
  ) {}

  @Post()
  request(@Headers() headers, @Body() body: TransferDto) {
    this.logger.log(`request`);
    this.logger.debug(`Headers: ${JSON.stringify(headers)}`);
    this.logger.debug(`Body: ${JSON.stringify(body)}`);
    const command = new TransferCommand(body);
    const currentSpan = this.traceService.getActiveSpan();
    currentSpan.setTag(`user`, command.from);
    return this.service.sendMoney(command);
  }

  @Get()
  get(@Headers() headers) {
    this.logger.log(`request`);
    this.logger.debug(`Headers: ${JSON.stringify(headers)}`);

    const randdomFail = Math.random() * 100;

    if (randdomFail > 50) {
      throw new Error('Random error');
    } else {
      return {
        id: '1',
        amount: 100,
        from: '123456789',
        to: '987654321',
      };
    }
  }
}
