import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { TransferCommand } from '../domain/transfer.command';
import { TransferPort } from '../domain/transfer.port';

import { Span, TraceService } from 'nestjs-ddtrace';

@Span()
@Injectable()
export class TransferAdapter implements TransferPort {
  private readonly logger = new Logger(TransferAdapter.name);
  constructor(private readonly traceService: TraceService) {}

  async sendMoney(command: TransferCommand) {
    const currentSpan = this.traceService.getActiveSpan();
    this.logger.log(`Send command ${JSON.stringify(command)}`);

    currentSpan.setTag(`user`, command.from);

    const userService = new Promise((resolve) => {
      resolve('123456789');
    });

    const userTracer = this.traceService
      .getTracer()
      .startSpan('sendfindUser', { childOf: currentSpan });
    userTracer.setTag(`user`, command.from);
    const user = await userService;
    if (command.from !== user) {
      userTracer.finish();
      throw new BadRequestException('Invalid user');
    }
    userTracer.finish();

    const amountTracer = this.traceService
      .getTracer()
      .startSpan('sendVerifyAmount', { childOf: currentSpan });
    amountTracer.setTag(`user`, command.from);
    const amountValidate = new Promise((resolve) => {
      if (command.amount > 1000) {
        resolve(false);
      } else {
        resolve(true);
      }
    });
    const amount = await amountValidate;
    if (!amount) {
      amountTracer.finish();
      throw new BadRequestException('Invalid amount');
    }
    amountTracer.finish();
    currentSpan.finish();
    return {
      from: command.from,
      to: command.to,
      amount: command.amount,
      description: command.description,
      accepted: true,
    };
  }
}
