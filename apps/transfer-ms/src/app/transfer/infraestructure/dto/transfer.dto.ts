import { TransferCommand } from '../../domain/transfer.command';

export class TransferDto implements TransferCommand {
  from: string;
  to: string;
  amount: number;
  description: string;
}
