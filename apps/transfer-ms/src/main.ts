/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */
import './datadog';
import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

import { AppModule } from './app/app.module';
import { Logger as PinoLogger } from 'nestjs-pino';
import {
  ReponseFailureFilter,
  ResponseSuccessInterceptor,
} from '@demoday/core';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.useLogger(app.get(PinoLogger));
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalInterceptors(new ResponseSuccessInterceptor());
  app.useGlobalFilters(new ReponseFailureFilter());
  const port = process.env.PORT || 9001;
  await app.listen(port);
  Logger.log(
    `🚀 Transfer-Ms is running on: http://localhost:${port}/${globalPrefix}`
  );
}

bootstrap();
